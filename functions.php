<?php
/*
 * __\(.?'(.*)'.?\)
 * __( '$1', TEXTDOMAIN )
 *
 *
 */

/**************************************************
 * load cpt
 **************************************************/
include_once('cpt/cpt_rezensionen.php');
include_once('cpt/cpt_interviews.php');
include_once('cpt/cpt_tagebuch.php');

/**************************************************
 * load dashboard widget
 **************************************************/
include_once('cpt/cpt_dashboard_widget.php');

/**************************************************
 * set theme language
 **************************************************/

define ('TEXTDOMAIN', 'schattenwege'); //default textdomain

add_action('after_setup_theme', 'SetThemeLanguage');
function SetThemeLanguage()
{
    load_theme_textdomain(TEXTDOMAIN, get_template_directory() . '/languages');
}

/**************************************************
 * rewrite flush after theme switch
 **************************************************/
add_action( 'after_switch_theme', 'my_rewrite_flush' );

function my_rewrite_flush()
{
    flush_rewrite_rules();
}

/**************************************************
 * load styles and scripts
 **************************************************/
add_action( 'admin_print_styles', 'load_custom_admin_css' );

function load_custom_admin_css()
{
    wp_enqueue_style('my_style', get_template_directory_uri() . '/css/admin.css');
}
// wp_register_style( $handle, $src, $deps, $ver, $media );

add_action( 'wp_enqueue_scripts', 'load_theme_scripts', 100 );

function load_theme_scripts()
{
    wp_register_style( 'schattenwege-styles', get_template_directory_uri() . '/css/style.css', array(), '1.7' );
    wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.1.0' );


    if ( !is_admin() )
    {
        wp_enqueue_style( 'schattenwege-styles' );
        wp_enqueue_style( 'font-awesome' );
        wp_enqueue_script( 'scrollUp', get_template_directory_uri() . '/js/jquery.scrollUp.min.js', array('jquery-core'), '2.4.0', true );
        // http://markgoodyear.com/2013/01/scrollup-jquery-plugin/
        wp_enqueue_script( 'schattenwege-scripts', get_template_directory_uri() . '/js/sw.js', array('jquery-core'), '1.0', true );
    }
}

add_action( 'init', 'my_theme_add_editor_styles' );
function my_theme_add_editor_styles() {
    //$font_url = 'http://fonts.googleapis.com/css?family=Lato:300,400,700';
    add_editor_style( 'css/editor.css' );
}



/**************************************************
 * allow shortcodes at widgets
 **************************************************/
add_filter('widget_text', 'do_shortcode');

/**************************************************
 * define sidebars
 **************************************************/
if (function_exists('register_sidebar'))
{
    register_sidebar(array(
        'name' => __( 'Aktionen', TEXTDOMAIN ),
        'id' => 'actionsidebar',
        'description' => __( 'Widgets werden auf jeder Seite mit Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'aktion_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Rezensionensidebar', TEXTDOMAIN ),
        'id' => 'rezensionensidebar',
        'description' => __( 'Widgets werden nur bei Rezensionen in der Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'post_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Tagebuchsidebar', TEXTDOMAIN ),
        'id' => 'tagebuchsidebar',
        'description' => __( 'Widgets werden nur bei Tagebucheinträgen in der Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'post_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Interviewsidebar', TEXTDOMAIN ),
        'id' => 'interviewsidebar',
        'description' => __( 'Widgets werden nur bei Interviews in der Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'post_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Standard', TEXTDOMAIN ),
        'id' => 'standard',
        'description' => __( 'Widgets werden auf jeder Seite mit Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'standard_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Home-Sidebar', TEXTDOMAIN ),
        'id' => 'homesidebar',
        'description' => __( 'Widgets werden nur auf der Startseiten-Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => '',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Artikelsidebar', TEXTDOMAIN ),
        'id' => 'postsidebar',
        'description' => __( 'Widgets werden nur bei Artikel in der Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'post_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Seitensidebar', TEXTDOMAIN ),
        'id' => 'pagesidebar',
        'description' => __( 'Widgets werden nur bei Seiten in der Sidebar angezeigt.', TEXTDOMAIN ),
        'class'         => 'page_sb',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Footer links', TEXTDOMAIN ),
        'id' => 'footer-left',
        'description' => __( 'Widgets werden links im Footer angezeigt.', TEXTDOMAIN ),
        'class'         => '',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Footer mitte', TEXTDOMAIN ),
        'id' => 'footer-center',
        'description' => __( 'Widgets werden mittig im Footer angezeigt.', TEXTDOMAIN ),
        'class'         => '',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div  class="h4 widget-title">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __( 'Footer rechts', TEXTDOMAIN ),
        'id' => 'footer-right',
        'description' => __( 'Widgets werden rechts im Footer angezeigt.', TEXTDOMAIN ),
        'class'         => '',
        'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<div class="h4 widget-title">',
        'after_title' => '</div>',
    ));
}

/**************************************************
 * helper - show sidebars
 * show_sidebars(name:string)
 **************************************************/
function show_sidebars ( $sidebar_ids )
{
    foreach ( $sidebar_ids as $sidebar_id )
    {
        if ( is_active_sidebar( $sidebar_id ) )
        {
            dynamic_sidebar( $sidebar_id );
        }
    }
}

/**************************************************
 * define menus
 **************************************************/
add_action( 'init', 'register_my_menus' );

function register_my_menus()
{
    register_nav_menus(
        array(
            'main-menu'     => __( 'Main Menu', TEXTDOMAIN ),
            'footer-menu'   => __( 'Footer Menu', TEXTDOMAIN ),
        )
    );
}

/**************************************************
 * theme support
 **************************************************/
add_theme_support( 'post-thumbnails' );
$custom_background_defaults = array(
	'default-color'          => '',
	'default-image'          => '',
	'wp-head-callback'       => '_custom_background_cb',
	'admin-head-callback'    => '',
	'admin-preview-callback' => ''
);
add_theme_support( 'custom-background', $custom_background_defaults );
$custom_header_defaults= array(
	'default-image'          => '',
	'random-default'         => false,
	'width'                  => 960,
	'height'                 => 200,
	'flex-height'            => false,
	'flex-width'             => false,
	'default-text-color'     => 'FFFFFF',
	'header-text'            => true,
	'uploads'                => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $custom_header_defaults );
add_theme_support( 'automatic-feed-links' );

/**************************************************
 * security & performance options
 **************************************************/
remove_action('wp_head', 'wp_generator');

/**************************************************
 * media
 **************************************************/
add_filter( 'post_mime_types', 'modify_post_mime_types' );

function modify_post_mime_types( $post_mime_types )
{
    // Mime Type auswählen: 'application/pdf'
    $post_mime_types['application/pdf'] = array(
        __( 'PDFs', TEXTDOMAIN ),
        __( 'PDFs Verwalten', TEXTDOMAIN ),
        _n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' )
    );

    return $post_mime_types;
}

/**************************************************
 * function to add custom post types to the rss feed
 **************************************************/
add_filter('request', 'myfeed_request');

function myfeed_request($qv) {
	if ( isset($qv['feed'] ) && !isset( $qv['post_type'] ) )
    {
        $qv['post_type'] = array('post', 'rezensionen', 'tagebuch', 'interviews' );
    }

	return $qv;
}

/**************************************************
 * imagessize
 **************************************************/
add_image_size( 'archive-thumb', '100', '100' , true );

/**************************************************
 * gesamtwertung errechnen - ungenutzt


function get_gesammtwertung( $data = array(), $interval = '0.5' )
{
    //.1 .2 .25 .5 und 1
    $anzahl_werte = count( $data );

    if ( $anzahl_werte == 0 )
    {
        $ausgabe = __( 'Keine Werte übergeben.', TEXTDOMAIN );
    }
    else {
        $faktor = 10/($interval*10);

        $summe = 0;

        foreach ($data as $item)
        {
            $summe += $item;
        }

        $wert = $summe/$anzahl_werte;

        $ausgabe = round( $wert * $faktor ) / $faktor;
    }

    return $ausgabe;
}
 **************************************************/


/**************************************************
 * helper - month/monat
 **************************************************/
function get_timestamp ( $year = '', $month = '', $day = '' )
{
    $m_de = array (
        'Januar',
        'Februar',
        'März',
        'April',
        'Mai',
        'Juni',
        'Juli',
        'August',
        'September',
        'Oktober',
        'November',
        'Dezember'
    );
    $m_en = array (
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );

    for ( $x = 0; $x < 12; $x++ )
    {
        $month = str_replace ( $m_de[$x], $m_en[$x], $month );
    }

    $string = $day . ' ' . $month  . ' ' . $year;

    $timestamp = strtotime( $string );

    return $timestamp;
}

/**************************************************
 * load external sources
 **************************************************/
include_once('inc/popluar-post.class.php');

/**************************************************
 * load header icons like favicon
 * favicon
 * shortcut icon
 * apple-touch-icon
 * apple-touch-icon-precomposed
 *
 * more information about it: http://en.wikipedia.org/wiki/Favicon
 **************************************************/
add_action('wp_head', 'load_header_icons');

function load_header_icons()
{
    include('inc/header-icons.php');
}

/**************************************************
 * Add specific CSS class by filter
 **************************************************/
add_filter('body_class','AddGuardianToBody');
add_filter('post_class', 'AddPostClass');

function AddGuardianToBody($classes) {
    // add 'class-name' to the $classes array
    $classes[] = 'verlauf';
    // return the $classes array
    return $classes;
}

// add category nicenames in body and post class
function AddPostClass($classes) {
    global $post;
    foreach((get_the_category($post->ID)) as $category)
        $classes[] = $category->category_nicename;
        $classes[] = 'content';
    return $classes;
}

//Funktion um Beitragsbilder im RSS-Feed anzuzeigen
function featured_image_in_rss($content)
{
    global $post;
    // Überprüfen, ob Artikel ein Beitragsbild hat
    if (has_post_thumbnail($post->ID))
    {
        $content = get_the_post_thumbnail($post->ID, 'full', array('style' => 'margin-bottom:10px;')) . $content;
    }
    return $content;
}
//Filter für RSS-Auszug
add_filter('the_excerpt_rss', 'featured_image_in_rss');
//Filter für RSS-Content
add_filter('the_content_feed', 'featured_image_in_rss');