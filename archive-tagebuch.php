<?php get_header(); ?>
<div id="main">
    <div class="col-1">
        <div class="content archiv">
            <div id="postingfeed">
                <?php
                if ( have_posts() ) {
                    while ( have_posts() ) : the_post();
                        ?>

                        <h3 class="<?php echo get_post_type( get_the_ID() ); ?>"><a href="<?php echo get_permalink(get_the_ID()); ?>"> <?php echo get_the_title(); ?></a></h3>

                        <?php include('parts/postmeta.php'); ?>

                        <?php include('parts/show-post-thumbnail-archive.php'); ?>

                        <?php
                        if ( function_exists( 'the_advanced_excerpt' ) ) {
                            the_advanced_excerpt('exclude_tags=img,hr');
                        }
                        ?>

                        <div class="clear postend"></div>

                    <?php endwhile; ?>

                <?php } elseif ( is_category() ) { ?>

                    <h2><?php echo __('Kategorie: ', TEXTDOMAIN) . single_cat_title( '', false) ?></h2>

                    <?php
                    $cat_id = get_cat_ID( single_cat_title( '', false) );

                    $args = array(
                        'post_type' => 'any',
                        'cat'  => $cat_id
                    );

                    $the_query = new WP_Query( $args );

                    if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>

                        <h3 class="<?php echo get_post_type( get_the_ID() ); ?>"><a href="<?php echo get_permalink(get_the_ID()); ?>"> <?php echo get_the_title(); ?></a></h3>

                        <?php include('parts/postmeta.php'); ?>

                        <?php include('parts/show-post-thumbnail-archive.php'); ?>

                        <?php
                        if (function_exists('the_advanced_excerpt')) {
                            the_advanced_excerpt();
                        }
                        ?>

                        <div class="clear postend"></div>

                    <?php endwhile; endif; wp_reset_postdata(); ?>

                    <?php include ('parts/page-nav.php'); ?>

                <?php } else { ?>

                    <h2>Archives by Month:</h2>
                    <ul>
                        <?php wp_get_archives('type=monthly'); ?>
                    </ul>

                    <h2>Archives by Subject:</h2>
                    <ul>
                        <?php wp_list_categories(); ?>
                    </ul>


                <?php } ?>

            </div>
            <?php include ('parts/page-nav.php'); ?>
        </div>

    </div>

    <div id="sidebar">
        <?php show_sidebars(array('actionsidebar', 'tagebuchsidebar', 'standard')); ?>
    </div>

    <div class="clear"></div>

</div>
<?php get_footer(); ?>