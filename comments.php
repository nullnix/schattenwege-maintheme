<?php

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { 
		echo '<p class="nocomments">' . __('Dieser Beitrag ist Passwortgeschützt. Bitte das Passwort eingeben.', TEXTDOMAIN) . '</p>';
		return;
	}
  
  // You can start editing here. -->
?>

<hr>
<?php if ( have_comments() ) : ?>
	
	<strong id="comments"><?php comments_number(__('Keine Kommentare', TEXTDOMAIN), __('eine Kommentar', TEXTDOMAIN), __('% Kommentare', TEXTDOMAIN) );?></strong>

	<div class="navigation">
		<div class="ta-left"><?php previous_comments_link() ?></div>
		<div class="ta-right"><?php next_comments_link() ?></div>
	</div>

	<ol class="commentlist">
    <?php wp_list_comments(array('avatar_size' => 32, 'reply_text' => '<i class="icon-reply fa fa-reply"></i> Antworten')); ?>
	</ol>
  
  <?php
    if ( is_user_logged_in() ) {
    ?>
  
  <ul class="comments dev">
  
  <?php
  /* nulli's comment bla */
    
  $comments = get_comments(array('post_id' => $post->ID, 'status' => 'approve', 'order' => 'ASC' ));
  
  foreach($comments as $comment) :
  	
    ?>
    <li class="comment">
      <div class="comment-avatar f-left"><?php echo get_avatar( $comment, 32 ); ?></div> 

      <div class="comment-author f-left"><?php comment_author(); ?> <?php echo __('sagt:', TEXTDOMAIN); ?> </div>

      <div class="clear"></div>

      <div class="comment-meta">
        <span><i class="icon-time"></i> <?php comment_date('j. F Y'); ?> <?php echo __('um', TEXTDOMAIN); ?> <?php comment_time('H:i');?> <?php echo __('Uhr', TEXTDOMAIN); ?></span>
      </div>
     
      <div class="comment-text"><?php comment_text(); ?></div>

      <div class="comment-aktion">Aktionen  <?php echo comment_reply_link(); ?></div>  
    </li>
    <?php /*  array('reply_text ' => 'Antworten'), $comment, $post->ID  */
  endforeach;
  
  ?>
  </ul>
  <?php      
    } 
  ?>
	<div class="navigation">
		<div class="ta-left"><?php previous_comments_link() ?></div>
		<div class="ta-right"><?php next_comments_link() ?></div>
	</div>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments">Comments are closed.</p>

	<?php endif; ?>
<?php endif; ?>


<?php if ( comments_open() ) : ?>

<div id="respond">

<h3><?php comment_form_title( 'Leave a Reply', 'Leave a Reply to %s' ); ?></h3>

<div class="cancel-comment-reply">
	<small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p>You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>">logged in</a> to post a comment.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( is_user_logged_in() ) : ?>

<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>

<?php else : ?>

<p><input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
<label for="author"><small>Name <?php if ($req) echo "(required)"; ?></small></label></p>

<p><input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
<label for="email"><small>Mail (will not be published) <?php if ($req) echo "(required)"; ?></small></label></p>

<p><input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
<label for="url"><small>Website</small></label></p>

<?php endif; ?>

<!--<p><small><strong>XHTML:</strong> You can use these tags: <code><?php echo allowed_tags(); ?></code></small></p>-->

<p><textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>

<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
<?php comment_id_fields(); ?>
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif;  ?>
</div>

<?php endif; ?>
