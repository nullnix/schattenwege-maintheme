<?php 
/* Template: CPT Interviews */
get_header(); ?>
    <div id="main">
        <div class="col-1">
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <h2 class="title"><?php the_title(); ?></h2>

                    <?php include('parts/postmeta.php'); ?>

                    <?php the_content(); ?>

                    <?php include('parts/postmeta-bottom.php'); ?>

                    <?php include('parts/post-nav.php');?>

                    <?php comments_template(); // Get wp-comments.php template ?>
                <?php endwhile; endif; ?>

            </div>
        </div>
        
        <div id="sidebar">
            <?php show_sidebars(array('actionsidebar', 'tagebuchsidebar', 'standard')); ?>
        </div>
        <div class="clear"></div>        
    </div>
<?php
if ( !is_user_logged_in() ) {
    Post_Views::increment_post_views(get_the_ID());
}
?>
<?php get_footer(); ?>           
