<?php get_header(); ?>
      <div id="main">
        <div class="col-1">
          <div class="content">
          
          <h2>404</h2>
          
          <h3>Leider wurde der von Dir gesuchte Inhalt nicht gefunden. Was Du jetzt tun kannst:</h3>
          <br>
          
          <h4>Zur Startseite gehen</h4>
          <a href="http://www.schattenwege.net" target="_self" title="Zur Startseite">Zur Startseite, und ein wenig stöbern</a><br>
          <br>
          
          <h4>Suchen</h4>
            <?php get_search_form(); ?>
          <br>
          
          <h4>Bescheid sagen</h4>
          <a href="http://www.schattenwege.net/kontakt" target="_self" title="Zum Kontaktformular">Du schreibst mir kurz was Du gesucht hast, vielleicht kann ich Dir direkt weiterhelfen.</a><br>
          </div>
        </div>
        
        <div id="sidebar">
          <?php show_sidebars(array('actionsidebar','postsidebar','standard')); ?>
        </div>
        <div class="clear"></div>        
      </div>
<?php get_footer(); ?>