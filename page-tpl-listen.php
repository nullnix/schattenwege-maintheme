<?php /* Template Name: Listen  */ get_header(); ?>
      <div id="main">
        <div class="col-1">
          <div class="content">
            <?php

            echo wp_count_posts('rezensionen')->publish;

            function custom_posts_per_tag($id, $post_type){
			
			  $args = array(
				'post_type' => array($post_type),
				'posts_per_page' => -1,
				'tag_id' => $id
			  );
			
			  $the_query = new WP_Query( $args );
			  wp_reset_query();
			
			  return sizeof($the_query->posts);
			}
			
			$tags = get_tags();
			
			global $wp_query;
			
			// $post_type = $wp_query->get('post_type');
			$post_type = 'rezensionen';
			
			foreach ($tags as $tag){
			echo $tag->term_id;
			/*    
			
			 if (custom_posts_per_tag($tag->term_id, $post_type) > 0) {
			        // Print tag link or whatever you wish here
			 		echo = $tag . '<br>';
			    }
			*/
			}
			
			//http://wordpress.org/support/topic/get-all-tags-used-in-custom-post-type
            /**/
            
            
            // Bücher nach Titel
            $args = array(
              'post_type'  => 'rezensionen',
              'order'      => 'ASC',
            );
            
            $the_query = new WP_Query( $args );
  
            if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
          ?>
            <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> von <?php echo get_post_meta ($post->ID, '_mb_rezension_autor', true); ?></h4>
          <?php  
            endwhile; 
            endif;
            wp_reset_postdata(); 
          ?>
          </div>  
        </div>
                
        <div id="sidebar">
          <?php show_sidebars(array('actionsidebar','pagesidebar','standard')); ?>
        </div>
        <div class="clear"></div>        
      </div>
<?php get_footer(); ?>