<?php get_header(); ?>

    <div id="main">
        <div class="col-1">
            <div class="content">
                <div id="postingfeed">
                    <?php if (have_posts()) : ?>
                        <h2 class="info"><span><?php echo $wp_query->found_posts; ?></span> <?php echo __('treffer zu ', TEXTDOMAIN); ?> <span>"<?php echo $s ?>"</span> <?php echo __('gefunden', TEXTDOMAIN);?></h2>
                        <?php while (have_posts()) : the_post(); ?>

                            <h3 class="<?php echo get_post_type( get_the_ID() ); ?>"><a href="<?php echo get_permalink(get_the_ID()); ?>"> <?php echo get_the_title(); ?></a></h3>

                            <?php include('parts/postmeta.php'); ?>

                            <?php
                            if (has_post_thumbnail()) {
                                ?>
                                <div class="post-thumbnail-container">
                                    <a href="<?php echo get_permalink(get_the_ID()); ?>" title="<?php echo get_the_title(); ?>">
                                        <?php
                                        $args = array(
                                            'alt'	=> trim(strip_tags( get_the_title() )),
                                            'title'	=> trim(strip_tags( get_the_title() )),
                                        );
                                        the_post_thumbnail('archive-thumb', $args); ?>
                                    </a>
                                </div>
                            <?php
                            }
                            ?>

                            <?php
                            if (function_exists('the_advanced_excerpt')) {
                                the_advanced_excerpt();
                            }
                            ?>

                            <div class="clear postend"></div>

                        <?php endwhile; ?>

                        <?php include ('parts/page-nav.php'); ?>

                        <?php else : ?>

                            <h2 class="info">Leider nichts gefunden :( sorry... nächstes Mal ist bestimmt was dabei! :)</h2>

                     <?php endif; ?>
                </div>
            </div>
        </div>
        
        <div id="sidebar">
            <?php show_sidebars(array('actionsidebar','pagesidebar','standard')); ?>
        </div>
    <div class="clear"></div>
</div>

<?php get_footer(); ?>