jQuery.noConflict();

jQuery(document).ready(function() {

    jQuery(function () {
        jQuery.scrollUp({
            scrollName: 'scrollUp', // Element ID
            topDistance: '300', // Distance from top before showing element (px)
            topSpeed: 300, // Speed back to top (ms)
            animation: 'fade', // Fade, slide, none
            animationInSpeed: 200, // Animation in speed (ms)
            animationOutSpeed: 200, // Animation out speed (ms)
            scrollText: 'Scroll to top', // Text for element
            scrollImg: true,
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
    });

    jQuery("form.opa50 input").focus(function () {
        jQuery("form.opa50").addClass("aktiv");
    });

    jQuery("form.opa50").focusout(function() {
        jQuery("form.opa50").removeClass("aktiv");
    });

});