<?php
get_header(); ?>

<div id="main">
    <div class="col-1">

        <div class="content">

            <div id="postingfeed">

                <?php /* query für alle postings */
                $args = array(
                    'post_type' => array(
                        'post',
                        'rezensionen',
                        'interviews',
                        'tagebuch',
                    ),
                    'orderby' => 'date',
                    'posts_per_page' => 7,
                    'post_status' => 'publish',
                    'category__not_in' => array(648)

                );
                // The Query
                $home_query = new WP_Query($args);

                // The Loop
                if ($home_query->have_posts()) {
                    while ($home_query->have_posts()) {
                        $home_query->the_post();

                        echo '<h3 class="' . get_post_type(get_the_ID()) . '"><a href="' . get_permalink(get_the_ID()) . '">' . get_the_title() . '</a></h3>';
                        ?>

                        <?php include('parts/postmeta.php'); ?>

                        <?php include('parts/show-post-thumbnail-archive.php'); ?>

                        <?php
                        if (function_exists('the_advanced_excerpt')) {
                            the_advanced_excerpt('exclude_tags=img,hr');
                        }
                        ?>

                        <?php
                        echo '<div class="clear postend"></div>';
                    }
                }
                /* Restore original Post Data */
                wp_reset_postdata();
                rewind_posts();

                ?>
            </div>
            <div class="startseite">
                <?php /* query für alle postings */
                $args = array(
                    'posts_per_page' => 1,
                    'post_status' => 'publish',
                    'page_id' => '125'
                );
                // The Query
                $start_query = new WP_Query($args);

                // The Loop
                if ($start_query->have_posts()) {
                    while ($start_query->have_posts()) {
                        $start_query->the_post();

                        the_content();

                        echo '<br class="clear">';
                    }
                }
                /* Restore original Post Data */
                wp_reset_postdata();
                rewind_posts();

                ?>
            </div>
        </div>
    </div>

    <div id="sidebar">

        <?php show_sidebars(array('actionsidebar', 'homesidebar', 'standard')); ?>

    </div>
    <div class="clear"></div>
</div>
<?php get_footer(); ?>      