<div class="postmeta">

    <span><i class="icon-time fa fa-clock-o"></i> <?php the_time('j. F Y - H:i:s'); ?></span>
    <span><i class="icon-user fa fa-user"></i> <?php the_author_meta('nickname'); ?></span>
    <span><a href="<?php echo get_permalink() ?>#comments" tilte="<?php the_title() ?>"><i class="icon-comments fa fa-comments"></i> <?php comments_number(__('Keine Kommentare', TEXTDOMAIN), __('ein Kommentar', TEXTDOMAIN), __('% Kommentare', TEXTDOMAIN) );?></a></span>

    <?php if( function_exists('zilla_likes') ) zilla_likes() ; ?>

    <?php if ( get_edit_post_link() ) { ?>
        <span class="loggedin"><a href="<?php echo get_edit_post_link(); ?>" title="<?php echo get_the_title() . ' ' . __('bearbeiten', TEXTDOMAIN); ?> "><i class="icon-edit fa fa-pencil-square-o "></i> <?php echo __('Bearbeiten', TEXTDOMAIN); ?></a></span>
    <?php } ?>
</div>