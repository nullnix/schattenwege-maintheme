<div class="social-buttons">
    <a rel="nofollow" href="https://www.facebook.com/schattenwege" target="_blank" title="Die Schattenwege bei Facebook - Jetzt Fan werden" class="fade t-shadow"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
    <a rel="nofollow" href="https://www.instagram.com/schattenwege" target="_blank" title="Die Schattenwege bei Instagram - Jetzt folgen" class="fade t-shadow"><i class="fa fa-instagram fa-fw fa-3x"></i></a>
    <a rel="nofollow" href="https://twitter.com/Schattenwege" target="_blank" title="Die Schattenwege bei Twitter - Jetzt folgen" class="fade t-shadow"><i class="fa fa-fw fa-twitter fa-3x"></i></a>
    <a rel="nofollow" href="http://www.schattenwege.net/feed/" target="_blank" title="Schattenwege RSS abonieren" class="fade t-shadow"><i class="fa fa-rss fa-fw fa-3x"></i></a>
</div>