<div class="postmeta-bottom" >

<?php if( function_exists('zilla_likes') ) zilla_likes(); ?>

<?php
    $categories = get_the_category();
    $separator = ' | ';
    $output = '';
    if($categories){
        foreach($categories as $category) {
            $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Alle Beiträge in %s anzeigen", TEXTDOMAIN ), $category->name ) ) . '">'.$category->cat_name.'</a>' . $separator;
        }
        echo '<i class="fa fa-folder"></i> ' . trim($output, $separator);
    }
?>
</div>