<hr>

<?php $args = array(
    'before'           => '<div id="pages-link">',
    'after'            => '</div>',
    'nextpagelink'     => __('Next page', TEXTDOMAIN),
    'previouspagelink' => __('Previous page', TEXTDOMAIN),
    'pagelink'         => 'Seite %',
);

wp_link_pages($args);
?>


<?php
$next = get_permalink(get_adjacent_post(false,'',false));
$id = get_adjacent_post(false,'',false);
if ($next != get_permalink()) {
    ?>
    <a class="post-nav prev fade" href="<?php echo $next; ?>" title="<?php echo get_the_title($id->ID); ?>"> <?php echo '<strong>' . __('Zurück zu: ', TEXTDOMAIN) . '</strong><br>'; ?> <?php echo get_the_title($id->ID); ?> </a> <span class="post-nav">|</span>
<?php
}
?>

<?php
$prev = get_permalink(get_adjacent_post(false,'',true));
$id = get_adjacent_post(false,'',true) ;
if ($prev != get_permalink()) {
    ?>
    <a class="post-nav next fade" href="<?php echo $prev; ?>" title="<?php echo get_the_title($id->ID); ?>"> <?php echo '<strong>' . __('Weiter mit: ', TEXTDOMAIN) . '</strong><br>'; ?>  <?php echo get_the_title($id->ID); ?> </a>
<?php
}
?>

<br class="clear">

<?php if(function_exists('selfserv_shareaholic')) { selfserv_shareaholic(); } ?>

<br>