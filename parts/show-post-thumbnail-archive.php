<?php if (has_post_thumbnail()) { ?>
    <div class="post-thumbnail-container">
        <a href="<?php echo get_permalink(get_the_ID()); ?>" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>">
            <?php
            $args = array(
                'alt'	=> trim(strip_tags( get_the_title() )),
                'title'	=> trim(strip_tags( get_the_title() )),
            );
            the_post_thumbnail('archive-thumb', $args); ?>
        </a>
    </div>
<?php } ?>