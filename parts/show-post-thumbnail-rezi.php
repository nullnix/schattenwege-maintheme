<?php if (has_post_thumbnail() && get_post_meta ($post->ID, '_mb_rezension_aktiv', true) == 'on' ) { ?>
    <div class="post-thumbnail-container">
        <?php
        $args = array(
            'alt'	=> trim(strip_tags( get_the_title() )),
            'title'	=> trim(strip_tags( get_the_title() )),
        );
        the_post_thumbnail('cpt-rezi-image', $args); ?>
    </div>
<?php } ?>