<?php get_header(); ?>
      <div id="main">
        <div class="col-1">
          <div class="content">          
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            
            <?php the_content(); ?>
            
            <?php endwhile; endif; ?>
          </div>  
        </div>
                
        <div id="sidebar">
          <?php show_sidebars(array('actionsidebar','pagesidebar','standard')); ?>
        </div>
        <div class="clear"></div>        
      </div>
<?php
    if ( !is_user_logged_in() ) {
    Post_Views::increment_post_views(get_the_ID());
    }
?>
<?php get_footer(); ?>