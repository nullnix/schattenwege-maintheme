<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta charset="<?php echo get_bloginfo('charset'); ?>">
        <meta name="description" content="Tagebuch, Rezensionen und Interviews">
        <meta name="keywords" content="buch, buecher, bücher, rezension, rezensionen, buecherwelten, interviews, tagebuch, gedanken, wortrausch, leben  ">
        <meta name="author" content="Jessica Idczak">

        <meta name="google-site-verification" content="b0F30g7yKTZZCFgfdJDzEB7TFi_v_LP2Du_fzxivaLA" >
        <meta name="alexaVerifyID" content="bvjfPtYDPT4kTxL4Am8fU7by01M" />

        <!-- wp head start-->
         <?php wp_head() ?>
        <!-- ep head end -->

        <title><?php echo get_bloginfo('name'); ?></title>

        <!--[if IE]>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css" />
        <![endif]-->

        <!--[if IE 9]>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie9.css" />
        <![endif]-->

        <!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie8.css" />
        <![endif]-->

        <!--[if IE 7]>
            <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.min.css">
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie7.css" />
        <![endif]-->

        <!--[if gt IE 6 & lte IE 9]>
            <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/pie.css" />
        <![endif]-->

    </head>

    <body <?php body_class(); ?>>

        <?php show_admin_bar( true ); ?>

        <div id="wrapper">
            <div id="header" class="page-container">

                <h1 class="no-display"><?php echo get_bloginfo('name'); ?></h1>
                <h2 class="no-display"><?php echo get_bloginfo('description'); ?></h2>

                <?php include( 'parts/social-buttons.php' ); ?>

                <?php get_search_form(); ?>

            </div>

            <div id="content" class="page-container">

                <div id="nav-container">
                    <?php
                    if ( has_nav_menu('main-menu') )
                    {
                        wp_nav_menu( array(
                            'theme_location' => 'main-menu',
                            'container' => false,
                        ));
                    }
                    ?>
                  <br class="clear">
                </div>
