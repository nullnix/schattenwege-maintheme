<?php
/*add custom post type */

add_action('init', 'create_cpt_rezensionen');

function create_cpt_rezensionen()
{
    register_post_type('rezensionen',
        array(
            'labels'      => array(
                'name'                => __( 'Rezensionen', TEXTDOMAIN ),
                'singular_name'       => __( 'Rezension', TEXTDOMAIN ),
                'add_new'             => __( 'Neu Rezension schreiben', TEXTDOMAIN ),
                'add_new_item'        => __( 'Neue Rezension', TEXTDOMAIN ),
                'edit_item'           => __( 'Rezension bearbeiten', TEXTDOMAIN ),
                'new_item'            => __( 'Neue Rezension', TEXTDOMAIN ),
                'all_items'           => __( 'Alle Rzension', TEXTDOMAIN ),
                'view_item'           => __( 'Rezension anzeigen', TEXTDOMAIN ),
                'search_items'        => __( 'Rezension suchen', TEXTDOMAIN ),
                'not_found'           => __( 'Keine passende Rezension gefunden', TEXTDOMAIN ),
                'not_found_in_trash'  => __( 'Keine Rezension im Papierkorb gefunden', TEXTDOMAIN ),
                'parent_item_colon'   => '',
                'menu_name'           => __( 'Rezensionen', TEXTDOMAIN )
            ),
            'public'              => true,
            'has_archive'         => true,
            'rewrite'             => array(
                'slug'  => 'rezensionen',
                'feeds' => true
            ),
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'capability_type'     => 'post',
            'hierarchical'        => false,
            'menu_position'       => 5,
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields', 'revisions'  ),
            //'menu_icon'           => '',
            'taxonomies' => array('post_tag', 'genre')
        )
    );
}

/* add custom taxonomie */

add_action( 'init', 'create_taxonomies_rezensionen', 0 );

function create_taxonomies_rezensionen()
{
    register_taxonomy( 'genre', 'rezensionen' ,
        $args = array(
            'labels'              => array(
                'name'                => _x( 'Genres', TEXTDOMAIN ),
                'singular_name'       => _x( 'Genre', TEXTDOMAIN ),
                'search_items'        => __( 'Genres durchsuchen', TEXTDOMAIN ),
                'all_items'           => __( 'Alle Genres', TEXTDOMAIN ),
                'parent_item'         => __( 'Übergeordnetes Genre', TEXTDOMAIN ),
                'parent_item_colon'   => __( 'Übergeordnetes Genre:', TEXTDOMAIN ),
                'edit_item'           => __( 'Genre bearbeiten', TEXTDOMAIN ),
                'update_item'         => __( 'Genre aktuallisieren', TEXTDOMAIN ),
                'add_new_item'        => __( 'Neues Genre', TEXTDOMAIN ),
                'new_item_name'       => __( 'Genre Name', TEXTDOMAIN ),
                'menu_name'           => __( 'Genre', TEXTDOMAIN )
            ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_admin_column'   => true,
            'show_in_nav_menus'   => true,
            'query_var'           => 'genre',
            'rewrite'             => array(
                'slug' => 'genre'
            ),
            'capabilities'        => array(
                'manage_terms'               => 'manage_categories',
                'edit_terms'                 => 'manage_categories',
                'delete_terms'               => 'manage_categories',
                'assign_terms'               => 'edit_posts',
            )
        )
    );
}

/* custom_fields */

add_action('add_meta_boxes', 'mb_rezension_infos_create');

function mb_rezension_infos_create()
{
    add_meta_box('mb_rezension_infos', 'Buchinformationen','mb_rezension_content', 'rezensionen', 'normal', 'high' );
}

function mb_rezension_content($post)
{
    $mb_rezension_aktiv                 = get_post_meta ($post->ID, '_mb_rezension_aktiv', true);           // Buchinfobox aktivieren
    $mb_rezension_autor                 = get_post_meta ($post->ID, '_mb_rezension_autor', true);           // Autor
    $mb_rezension_autor_nn              = get_post_meta ($post->ID, '_mb_rezension_autor_nn', true);        // Autor Nachname
    $mb_rezension_titel                 = get_post_meta ($post->ID, '_mb_rezension_titel', true);           // Titel
    $mb_rezension_verlag                = get_post_meta ($post->ID, '_mb_rezension_verlag', true);          // Verlag
    $mb_rezension_auflage               = get_post_meta ($post->ID, '_mb_rezension_auflage', true);         // Auflage
    $mb_rezension_datum                 = get_post_meta ($post->ID, '_mb_rezension_datum', true);           // Datum(Monat Jahr)
    $mb_rezension_timestamp             = get_post_meta ($post->ID, '_mb_rezension_timestamp', true);       // Timestamp von Datum
    $mb_rezension_originaltitel         = get_post_meta ($post->ID, '_mb_rezension_originaltitel', true);   // Originaltitel (optional)
    $mb_rezension_uebersetzer           = get_post_meta ($post->ID, '_mb_rezension_uebersetzer', true);     // Übersetzer (optional)
    $mb_rezension_art                   = get_post_meta ($post->ID, '_mb_rezension_art', true);             // Art (Checkbox)
    $mb_rezension_seitenzahl            = get_post_meta ($post->ID, '_mb_rezension_seitenzahl', true);      // Seiten
    $mb_rezension_extras                = get_post_meta ($post->ID, '_mb_rezension_extras', true);          // Extras
    $mb_rezension_preise                = get_post_meta ($post->ID, '_mb_rezension_preise', true);          // Preis DE AT CH
    $mb_rezension_isbn                  = get_post_meta ($post->ID, '_mb_rezension_isbn', true);            // ISBN
    $mb_rezension_leseprobe             = get_post_meta ($post->ID, '_mb_rezension_leseprobe', true);       // Leseprobe (Upload)
    $mb_rezension_genre                 = get_post_meta ($post->ID, '_mb_rezension_genre', true);           // Genre (Kategorie oder Text)
    $mb_rezension_b_freitext            = get_post_meta ($post->ID, '_mb_rezension_b_freitext', true);      // bewertung freitext
    $mb_rezension_b_handlung            = get_post_meta ($post->ID, '_mb_rezension_b_handlung', true);      // bewertung handlung
    $mb_rezension_b_charaktere          = get_post_meta ($post->ID, '_mb_rezension_b_charaktere', true);    // bewertung charaktere
    $mb_rezension_b_lesespass           = get_post_meta ($post->ID, '_mb_rezension_b_lesespass', true);     // bewertung lesespaß
    $mb_rezension_b_preisleistung       = get_post_meta ($post->ID, '_mb_rezension_b_preisleistung', true); // bewertung preise leistung
    $mb_rezension_b_lyrik               = get_post_meta ($post->ID, '_mb_rezension_b_lyrik', true);         // bewertung preise leistung
    $mb_rezension_b_informationsgehalt  = get_post_meta ($post->ID, '_mb_rezension_b_informationsgehalt', true);        // bewertung Informationsgehalt
    $mb_rezension_b_verstaendlichkeit   = get_post_meta ($post->ID, '_mb_rezension_b_verstaendlichkeit', true);         // bewertung Verständlichkeit
    $mb_rezension_b_all                 = get_post_meta ($post->ID, '_mb_rezension_b_all', true);           // bewertung gesammt
?>

    <div class="buchinfo-item">
        <label for="mb_rezension_aktiv"><?php echo __( 'Buchinfobox aktivieren: ', TEXTDOMAIN ); ?></label>
        <input type="radio" id="mb_rezension_aktiv_on" name="mb_rezension_aktiv" <?php if($mb_rezension_aktiv == 'on') echo 'checked="checked"'; ?> value="on" onClick="jQuery('#buchinfo').removeClass('no-display');"> Ja
        <input type="radio" id="mb_rezension_aktiv_off" name="mb_rezension_aktiv" <?php if($mb_rezension_aktiv != 'on') echo 'checked="checked"'; ?> value="off" onClick="jQuery('#buchinfo').addClass('no-display');"> Nein
    </div>

    <div id="buchinfo" class="<?php if($mb_rezension_aktiv != 'on') echo 'no-display'; ?>">
        <strong>Allgemeine Informationen</strong>
        <hr>

        <div class="buchinfo-item">
            <label for="mb_rezension_titel"><?php echo __( 'Titel: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_titel" name="mb_rezension_titel" value="<?php echo esc_attr( $mb_rezension_titel );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_autor"><?php echo __( 'Autor: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_autor" name="mb_rezension_autor" value="<?php echo esc_attr( $mb_rezension_autor );?>"/>

            <?php
                $pattern = '@(.*) (.*)@is'; //Preise, Werte
                $result = preg_match($pattern, $mb_rezension_autor, $subpattern);
                $mb_rezension_autor_nn = $subpattern[2];
                if (count($subpattern) == 0)
                {
                    $mb_rezension_autor_nn = $mb_rezension_autor;
                };
            ?>

            <input type="hidden" id="mb_rezension_autor_nn" name="mb_rezension_autor_nn" value="<?php echo esc_attr( $mb_rezension_autor_nn );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_verlag"><?php echo __( 'Verlag: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_verlag" name="mb_rezension_verlag" value="<?php echo esc_attr( $mb_rezension_verlag );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_auflage"><?php echo __( 'Auflage: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_auflage" name="mb_rezension_auflage" value="<?php echo esc_attr( $mb_rezension_auflage );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_datum"><?php echo __( 'Datum: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_datum" name="mb_rezension_datum" value="<?php echo esc_attr( $mb_rezension_datum );?>"/>

            <?php
                $pattern = '@(.*)\s(....)@iU';
                $result = preg_match($pattern, $mb_rezension_datum, $subpattern);
                $timestamp = get_timestamp($subpattern[2], $subpattern[1]);
            ?>

            <input type="hidden" id="mb_rezension_timestamp" name="mb_rezension_timestamp" value="<?php echo esc_attr( $timestamp );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_originaltitel"><?php echo __( 'Originaltitel: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_originaltitel" name="mb_rezension_originaltitel" value="<?php echo esc_attr( $mb_rezension_originaltitel );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_uebersetzer"><?php echo __( 'Übersetzt von: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_uebersetzer" name="mb_rezension_uebersetzer" value="<?php echo esc_attr( $mb_rezension_uebersetzer );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_art"><?php echo __( 'Cover: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_art" name="mb_rezension_art" value="<?php echo esc_attr( $mb_rezension_art );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_seitenzahle"><?php echo __( 'Seitenzahl: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_seitenzahl" name="mb_rezension_seitenzahl" value="<?php echo esc_attr( $mb_rezension_seitenzahl );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_extras"><?php echo __( 'Extras: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_extras" name="mb_rezension_extras" value="<?php echo esc_attr( $mb_rezension_extras );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_preise"><?php echo __( 'Preise: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_preise" name="mb_rezension_preise" value="<?php echo esc_attr( $mb_rezension_preise );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_isbn"><?php echo __( 'ISBN: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_isbn" name="mb_rezension_isbn" value="<?php echo esc_attr( $mb_rezension_isbn );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_genre"><?php echo __( 'Genre: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_genre" name="mb_rezension_genre" value="<?php echo esc_attr( $mb_rezension_genre );?>"/>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_leseprobe"><?php echo __( 'Leseprobe: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_leseprobe" name="mb_rezension_leseprobe" value="<?php echo esc_attr( $mb_rezension_leseprobe );?>"/>
        </div>

        <div class="clear"></div>
        <br>

        <strong>Bewertungen</strong>
        <hr>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_handlung"><?php echo __( 'Handlung: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_handlung" name="mb_rezension_b_handlung">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_handlung ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_charaktere"><?php echo __( 'Charaktere: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_charaktere" name="mb_rezension_b_charaktere">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_charaktere ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_lesespass"><?php echo __( 'Lesespaß: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_lesespass" name="mb_rezension_b_lesespass">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_lesespass ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_preisleistung"><?php echo __( 'Preis/Leistung: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_preisleistung" name="mb_rezension_b_preisleistung">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_preisleistung ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_lyrik"><?php echo __( 'Lyrik: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_lyrik" name="mb_rezension_b_lyrik">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_lyrik ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_informationsgehalt"><?php echo __( 'Informationsgehalt: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_informationsgehalt" name="mb_rezension_b_informationsgehalt">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_informationsgehalt ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_verstaendlichkeit"><?php echo __( 'Verständlichkeit: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_verstaendlichkeit" name="mb_rezension_b_verstaendlichkeit">

            <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_verstaendlichkeit ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
            <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_all"><?php echo __( 'Gesamtbewertung: ', TEXTDOMAIN ); ?></label>
            <select id="mb_rezension_b_all" name="mb_rezension_b_all">

                <?php for ($c = 0; $c <= 5.0 ; $c = $c + 0.5) { ?>
                    <option value="<?php echo $c; ?>" <?php if ( esc_attr( $mb_rezension_b_all ) == $c) : echo 'selected'; endif; ?>><?php echo $c; ?></option>
                <?php } ?>

            </select>
        </div>

        <div class="buchinfo-item">
            <label for="mb_rezension_b_freitext"><?php echo __( 'Freitext: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_rezension_b_freitext" name="mb_rezension_b_freitext" value="<?php echo esc_attr( $mb_rezension_b_freitext );?>"/>
        </div>
        <div class="clear"></div>
        <br>

        <strong>Entwicklerdaten</strong>
        <hr>

        <div id="entwicklerdaten">
            <?php
            if ( isset($mb_rezension_timestamp) )
            {
                echo '<br>Timestamp: ' . $mb_rezension_timestamp;
                echo '<br>Datum: '. date( 'm Y', $mb_rezension_timestamp );
            }

            if ( isset($mb_rezension_autor_nn) )
            {
                echo '<br>Autor NN: ' . $mb_rezension_autor_nn;
            }
            ?>
        </div>

        <br><br>

        <?php
        if ( empty( $mb_rezension_titel ) )
        {
            $pattern = array();
            $pattern['titel-autor'] = '@(.*)?[\(](.*)[\)]@is'; //titel, autor
            $pattern['verlag-auflage-datum'] = '@(.*), (\d.\sAuflage) (.*\d{4})@iU';  //Verlag, Auflage, Erscheinungsdatum
            $pattern['cover-seiten'] = '@(.*), (\d* Seiten)@iU'; //Cover, Seiten
            $pattern['preis-isbn-titel'] = '@(.*): (.*$)@iU'; //Preise, Werte
            $pattern['autor_nn'] = '@(.*) (.*)@is'; //Preise, Werte

            $subject = get_the_title();

            $result = preg_match($pattern['titel-autor'], $subject, $subpattern);
            $titel = $subpattern[1];
            $autor = $subpattern[2];

            //var_dump($subpattern);

            $content =  strip_tags ( str_replace( '<br />', "\n", substr ( $post->post_content, 0, strpos( $post->post_content, '<hr />' ) ) ) );
            $content =  strip_tags ( str_replace( "\r", "\n", $content ) );

            $content_array_clean = array_values(
                array_filter(
                    explode( "\n", $content )
                )
            );

            $count = count( $content_array_clean );

            if ( $count == 5 || $count == 6 )
            {
                $result = preg_match($pattern['verlag-auflage-datum'], $content_array_clean[0], $subpattern);
                $verlag = $subpattern[1];
                $auflage = $subpattern[2];
                $datum = $subpattern[3];

                $result = preg_match($pattern['cover-seiten'], $content_array_clean[1], $subpattern);
                $cover = $subpattern[1];
                $seiten = $subpattern[2];

                $result = preg_match($pattern['preis-isbn-titel'], $content_array_clean[2], $subpattern);
                $preise = $content_array_clean[2];

                $result = preg_match($pattern['preis-isbn-titel'], $content_array_clean[3], $subpattern);
                $isbn = $subpattern[2];
            }
            else if ( $count == 7 || $count == 8  || $count == 9 )
            {
                $result = preg_match($pattern['verlag-auflage-datum'], $content_array_clean[0], $subpattern);
                $verlag = $subpattern[1];
                $auflage = $subpattern[2];
                $datum = $subpattern[3];

                $result = preg_match($pattern['preis-isbn-titel'], $content_array_clean[1], $subpattern);
                $o_title = $subpattern[2];

                $von = $content_array_clean[2];

                $result = preg_match($pattern['cover-seiten'], $content_array_clean[3], $subpattern);
                $cover = $subpattern[1];
                $seiten = $subpattern[2];

                $result = preg_match($pattern['preis-isbn-titel'], $content_array_clean[4], $subpattern);
                $preise = $content_array_clean[4];

                $result = preg_match($pattern['preis-isbn-titel'], $content_array_clean[5], $subpattern);
                $isbn = $content_array_clean[5];
            }
            else
            {
                echo $fail = '<strong>Einmal mit Profis... Bitte informieren Sie Ihren SiteAdmin.</strong><br><br>';
            }
        ?>

        <script type="text/javascript">
            jQuery.fn.get_old_data = function () {
                jQuery('#mb_rezension_titel').val('<?php echo $titel ?>');
                jQuery('#mb_rezension_autor').val('<?php echo $autor ?>');
                jQuery('#mb_rezension_verlag').val('<?php echo $verlag ?>');
                jQuery('#mb_rezension_auflage').val('<?php echo $auflage ?>');
                jQuery('#mb_rezension_datum').val('<?php echo $datum ?>');
                jQuery('#mb_rezension_originaltitel').val('<?php echo $o_title ?>');
                jQuery('#mb_rezension_uebersetzer').val('<?php echo $von ?>');
                jQuery('#mb_rezension_art').val('<?php echo $cover ?>');
                jQuery('#mb_rezension_seitenzahl').val('<?php echo $seiten ?>');
                jQuery('#mb_rezension_preise').val('<?php echo $preise ?>');
                jQuery('#mb_rezension_isbn').val('<?php echo $isbn ?>');
            };
        </script>

        <?php
            if ( empty($fail) )
            {
        ?>
            <a id="get_old_data" onclick="jQuery('this').get_old_data();">Daten übernehmen</a>
        <?php
            }
            echo '<br><pre>';
            var_dump( $content_array_clean );
            echo '</pre>';
        }
        ?>
    </div>
<?php
}

/* save meta */
add_action ('save_post', 'mb_rezension_save_meta');

function mb_rezension_save_meta( $post_id )
{
    if (isset($_POST['mb_rezension_aktiv'])) {
        update_post_meta($post_id, '_mb_rezension_aktiv', strip_tags($_POST['mb_rezension_aktiv']));
    }

    if (isset($_POST['mb_rezension_autor'])){
        update_post_meta($post_id, '_mb_rezension_autor', strip_tags($_POST['mb_rezension_autor']));
    }

    if (isset($_POST['mb_rezension_autor_nn'])){
        update_post_meta($post_id, '_mb_rezension_autor_nn', strip_tags($_POST['mb_rezension_autor_nn']));
    }

    if (isset($_POST['mb_rezension_titel'])){
        update_post_meta($post_id, '_mb_rezension_titel', strip_tags($_POST['mb_rezension_titel']));
    }

    if (isset($_POST['mb_rezension_verlag'])){
        update_post_meta($post_id, '_mb_rezension_verlag', strip_tags($_POST['mb_rezension_verlag']));
    }

    if (isset($_POST['mb_rezension_auflage'])){
        update_post_meta($post_id, '_mb_rezension_auflage', strip_tags($_POST['mb_rezension_auflage']));
    }

    if (isset($_POST['mb_rezension_datum'])){
        update_post_meta($post_id, '_mb_rezension_datum', strip_tags($_POST['mb_rezension_datum']));
    }

    if (isset($_POST['mb_rezension_timestamp'])){
        update_post_meta($post_id, '_mb_rezension_timestamp', strip_tags($_POST['mb_rezension_timestamp']));
    }

    if (isset($_POST['mb_rezension_originaltitel'])){
        update_post_meta($post_id, '_mb_rezension_originaltitel', strip_tags($_POST['mb_rezension_originaltitel']));
    }

    if (isset($_POST['mb_rezension_uebersetzer'])){
        update_post_meta($post_id, '_mb_rezension_uebersetzer', strip_tags($_POST['mb_rezension_uebersetzer']));
    }

    if (isset($_POST['mb_rezension_art'])){
        update_post_meta($post_id, '_mb_rezension_art', strip_tags($_POST['mb_rezension_art']));
    }

    if (isset($_POST['mb_rezension_seitenzahl'])){
        update_post_meta($post_id, '_mb_rezension_seitenzahl', strip_tags($_POST['mb_rezension_seitenzahl']));
    }

    if (isset($_POST['mb_rezension_extras'])){
        update_post_meta($post_id, '_mb_rezension_extras', strip_tags($_POST['mb_rezension_extras']));
    }

    if (isset($_POST['mb_rezension_preise'])){
        update_post_meta($post_id, '_mb_rezension_preise', strip_tags($_POST['mb_rezension_preise']));
    }

    if (isset($_POST['mb_rezension_isbn'])){
        update_post_meta($post_id, '_mb_rezension_isbn', strip_tags($_POST['mb_rezension_isbn']));
    }

    if (isset($_POST['mb_rezension_leseprobe'])){
        update_post_meta($post_id, '_mb_rezension_leseprobe', strip_tags($_POST['mb_rezension_leseprobe']));
    }

    if (isset($_POST['mb_rezension_genre'])){
        update_post_meta($post_id, '_mb_rezension_genre', strip_tags($_POST['mb_rezension_genre']));
    }

    if (isset($_POST['mb_rezension_b_freitext'])){
        update_post_meta($post_id, '_mb_rezension_b_freitext', strip_tags($_POST['mb_rezension_b_freitext']));
    }

    if (isset($_POST['mb_rezension_b_handlung'])){
        update_post_meta($post_id, '_mb_rezension_b_handlung', strip_tags($_POST['mb_rezension_b_handlung']));
    }

    if (isset($_POST['mb_rezension_b_charaktere'])){
        update_post_meta($post_id, '_mb_rezension_b_charaktere', strip_tags($_POST['mb_rezension_b_charaktere']));
    }

    if (isset($_POST['mb_rezension_b_lesespass'])){
        update_post_meta($post_id, '_mb_rezension_b_lesespass', strip_tags($_POST['mb_rezension_b_lesespass']));
    }

    if (isset($_POST['mb_rezension_b_preisleistung'])){
        update_post_meta($post_id, '_mb_rezension_b_preisleistung', strip_tags($_POST['mb_rezension_b_preisleistung']));
    }

    if (isset($_POST['mb_rezension_b_informationsgehalt'])){
        update_post_meta($post_id, '_mb_rezension_b_informationsgehalt', strip_tags($_POST['mb_rezension_b_informationsgehalt']));
     }

    if (isset($_POST['mb_rezension_b_verstaendlichkeit'])){
        update_post_meta($post_id, '_mb_rezension_b_verstaendlichkeit', strip_tags($_POST['mb_rezension_b_verstaendlichkeit']));
    }

    if (isset($_POST['mb_rezension_b_lyrik'])){
        update_post_meta($post_id, '_mb_rezension_b_lyrik', strip_tags($_POST['mb_rezension_b_lyrik']));
    }

    if (isset($_POST['mb_rezension_b_all'])){
        update_post_meta($post_id, '_mb_rezension_b_all', strip_tags($_POST['mb_rezension_b_all']));
    }

}

/* RSS */

/* shortcodes */

add_shortcode('zeige-gelesene-seiten', 'zeige_gelesene_seiten');
function zeige_gelesene_seiten(  )
{
    $seiten = '0';

    $args = array(
        'post_type'  => 'rezensionen',
        'meta_key'   => '_mb_rezension_seitenzahl',
        'meta_query' => array(
            array(
                'key'   => '_mb_rezension_seitenzahl',
                'value' => '',
                'compare' => '!='
            )
        ),
        'posts_per_page' => '-1',
    );

    $query = new WP_Query($args);

    while ( $query->have_posts() ) {
        $query->the_post();

        $buchseiten = get_post_meta (get_the_ID(), '_mb_rezension_seitenzahl', true);

        $seiten = $seiten + $buchseiten;
    }
    wp_reset_postdata();

    $seiten;

    return $seiten;
}

add_shortcode('zeige-rezensionen', 'zeige_rezensionen');
function zeige_rezensionen( $atts )
{

    $output = '';
    $titel = '';
    $typ = '';
    $autor = '';

    extract( shortcode_atts( array(
        'typ'   => '', // bewertungen, titel, autor, genre
        'titel' => 'default',
        'limit' => '0',
        'autor' => '',
    ),
    $atts ) );


    switch( $typ )
    {
        case 'bewertungen':

        // Bücher nach Bewertung
        if ( $titel != 'default' )
        {
            $output .= '<h3>' . $titel . '</h3>';
        }
        else
        {
            $output .= '<h3>Bücher sortiert nach Bewertung</h3>';
        }

        for( $count = 5; $count > 0; $count=$count-0.5 )
        {
            $args = array(
                'post_type'  => 'rezensionen',
                'meta_key'   => '_mb_rezension_b_all',
                'orderby'    => 'title',
                'order'      => 'ASC',
                'meta_query' => array(
                    array(
                        'key'   => '_mb_rezension_b_all',
                        'value' => $count,
                    )
                ),
                'posts_per_page' => '-1',
            );

            $the_query = new WP_Query( $args );

            $count2 = 0;

            if ( $the_query->have_posts() )
            {
                while ( $the_query->have_posts() )
                {
                    $the_query->the_post();
                    $count2++;
                }
            }


            if ( $count2 > 0 )
            {
                $output .= '<h4 class="">Bücher mit einer Gesamtwertung mit ' . $count . ' von 5</h4>';
                $output .= '<ul class="'.$typ.'">';
            }

            if ( $the_query->have_posts() )
            {
                while ($the_query->have_posts())
                {
                    $the_query->the_post();
                    $output .= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
                }
                $output .= '</ul>';
            }

            wp_reset_postdata();

        }

        break;

        case 'titel':

        $args = array(
            'post_type'  => 'rezensionen',
            'meta_key'   => '_mb_rezension_titel',
            'meta_query' => array(
                array(
    			    'key' => '_mb_rezension_titel',
			        'value' => '',
			        'compare' => '!='
		        )
	        ),
            'orderby'    => 'meta_value',
            'order'      => 'ASC',
            'posts_per_page' => '-1',
        );

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() )
        {

            // Bücher nach Titel
            if ( $titel != 'default' )
            {
                $output .= '<h3>' . $titel . '</h3>';
                $output .= '<ul class="'.$typ.'">';
            }
            else
            {
                $output .= '<h3>Bücher sortiert nach Titel</h3>';
                $output .= '<ul class="'.$typ.'">';
            }

            $current_letter = '';
            while ( $the_query->have_posts() )
            {
                $the_query->the_post();

                $letter = substr( get_post_meta (get_the_ID(), '_mb_rezension_titel', true), 0, 1 );

                if ( $letter != $current_letter )
                {
                    $current_letter = $letter;

                    $letters[] = substr( get_post_meta (get_the_ID(), '_mb_rezension_titel', true), 0, 1 );
                }

            }

            $output .= '<div>';

            foreach ( $letters as $letter )
            {
                $output .= ' <a href="#' . $letter . '">' . $letter .'</a> ';
            }

            $output .= '</div>';

            $current_letter = '';
            while ( $the_query->have_posts() )
            {
                $the_query->the_post();

                if ( get_post_meta( get_the_ID(), '_mb_rezension_titel', true ) )
                {
                    $letter = substr( get_post_meta (get_the_ID(), '_mb_rezension_titel', true), 0, 1 );

                    if ( $letter != $current_letter )
                    {
                        $current_letter = $letter;

                        $output .= '<li><h4 id="' . $current_letter . '" class="letter">';

                        if ( preg_match('#^[a-z]+$#i', $current_letter) )
                        {
                            $file = $current_letter;
                            $file = strtolower( $file );
                        }
                        else
                        {
                            $file = 'raute';
                        }

                        $output .= '<img class="no-lazy" src="' . get_template_directory_uri() . '/images/letter/' . $file . '.png" alt="' . $current_letter . '" title="' . $current_letter . '">';
                        $output .= '</h4></li>';
                    }

                    $output .= '<li><a href="' . get_permalink() .'">' . get_post_meta (get_the_ID(), '_mb_rezension_titel', true) . ' von ' . get_post_meta (get_the_ID(), '_mb_rezension_autor', true) . '</a></li>';
                }

            }

            $output .= '</ul>';
        }

        wp_reset_postdata();

        break;

        case 'autor':

        $args = array(
            'post_type'  => 'rezensionen',
            'meta_key'   => '_mb_rezension_autor_nn',
            'meta_query' => array(
                array(
    			    'key' => '_mb_rezension_autor_nn',
			        'value' => '',
			        'compare' => '!='
		        )
	        ),
            'orderby'    => 'meta_value',
            'order'      => 'ASC',
            'posts_per_page' => '-1',
        );

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() )
        {
            // Bücher nach Autor
            if ( $titel != 'default' )
            {
                $output .= '<h3>' . $titel . '</h3>';
                $output .= '<ul class="'.$typ.'">';
            }
            else
            {
                $output .= '<h3>Bücher sortiert nach Autor</h3>';
                $output .= '<ul class="'.$typ.'">';
            }

            $current_letter = '';
            while ( $the_query->have_posts() )
            {
                $the_query->the_post();

                $letter = substr( get_post_meta (get_the_ID(), '_mb_rezension_autor_nn', true), 0, 1 );

                if ( $letter != $current_letter )
                {
                    $current_letter = $letter;

                    $letters[] = substr( get_post_meta (get_the_ID(), '_mb_rezension_autor_nn', true), 0, 1 );
                }

            }

            $output .= '<div>';

            foreach ( $letters as $letter )
            {
                $output .= ' <a href="#' . $letter . '">' . $letter .'</a> ';
            }

            $output .= '</div>';

            $current_letter = '';
            while ( $the_query->have_posts() )
            {
                $the_query->the_post();

                if ( get_post_meta( get_the_ID(), '_mb_rezension_autor', true ) )
                {
                    $letter = substr( get_post_meta (get_the_ID(), '_mb_rezension_autor_nn', true), 0, 1 );

                    if ( $letter != $current_letter )
                    {
                        $current_letter = $letter;

                        $output .= '<li><h4 id="' . $current_letter . '" class="letter">';

                        if ( preg_match('#^[a-z]+$#i', $current_letter) )
                        {
                            $file = $current_letter;
                            $file = strtolower( $file );
                        }
                        else
                        {
                            $file = 'raute';
                        }

                        $output .= '<img class="no-lazy" src="' . get_template_directory_uri() . '/images/letter/' . $file . '.png" alt="' . $current_letter . '" title="' . $current_letter . '">';
                        $output .= '</h4></li>';
                    }

                    $output .= '<li><a href="' . get_permalink() .'">' . get_post_meta (get_the_ID(), '_mb_rezension_autor', true) . ': ' . get_post_meta (get_the_ID(), '_mb_rezension_titel', true) . '</a></li>';
                }

            }

            $output .= '</ul>';
        }

        wp_reset_postdata();

        break;

        case 'genre':

            $genres = get_terms( 'genre' );

            if ( !empty( $genres ) && ! is_wp_error( $genres ) ) {

                // Bücher nach Genre
                if ( $titel != 'default' )
                {
                    $output .= '<h3>' . $titel . '</h3>';
                    $output .= '<ul class="'.$typ.'">';
                }
                else
                {
                    $output .= '<h3>Bücher sortiert nach Genre</h3>';
                    $output .= '<ul class="'.$typ.'">';
                }

                foreach ( $genres as $genre ) {

                    $args = array(
                        'post_type'  => 'rezensionen',
                        'tax_query'  => array(
                            array(
                                'taxonomy' => 'genre',
                                'field'    => 'slug',
                                'terms'    => $genre->slug,
                            ),
                        ),
                        'meta_key'   => '_mb_rezension_titel',
                        'orderby'    => 'meta_value',
                        'order'      => 'ASC',
                        'posts_per_page' => '-1',
                    );


                    $the_query = new WP_Query( $args );

                    if ( $the_query->have_posts() ) {

                        $output .=  '<li><h4>' . $genre->name . '</h4>';

                        $output .=  '<ul>';

                        while ( $the_query->have_posts() )
                        {
                            $the_query->the_post();

                            $output .=   '<li><a href="' . get_permalink() .'">' . get_post_meta (get_the_ID(), '_mb_rezension_titel', true) . '</a></li>';
                        }
                        $output .= '</ul></li>';
                    }

                }
                $output .= '</ul>';

            }

        wp_reset_postdata();

        break;

        default:

        $output .= 'Du hast irgendwie den falschen Typ angegeben. autor, titel, bewertungen oder genre sind gültige Typen.';
        break;
  }

  return $output;
}

/* imagessize */

add_image_size( 'cpt-rezi-image', '200', '275', false );
add_image_size( 'cpt-rezi-image-related', '120', '160', true );

/* ADD NEW COLUMN */

// add new column
add_filter( 'manage_rezensionen_posts_columns', 'rezensionen_columns_head' );
// make sort order sortable
add_filter( 'manage_edit-rezensionen_sortable_columns', 'rezensionen_columns_head' );
function rezensionen_columns_head( $defaults )
{
    $defaults['buch_info'] = 'Buch-Info';
    $defaults['buch_autor'] = 'Autor';
    $defaults['buch_erscheinungsdatum'] = 'Erscheinungsdatum';
    return $defaults;
}

// new column content
add_action( 'manage_rezensionen_posts_custom_column', 'rezensionen_columns_content', 10, 2 );
function rezensionen_columns_content( $column_name, $post_ID )
{
    if ( $column_name == 'buch_info' )
    {
        $mb_rezension_aktiv = get_post_meta( $post_ID, '_mb_rezension_aktiv', true );
        if ( $mb_rezension_aktiv == 'on' )
        {
            echo __( 'Ja', TEXTDOMAIN );
        }
        else
        {
            echo __( 'Nein', TEXTDOMAIN );
        }
    }

    if ( $column_name == 'buch_autor' )
    {
        if ( get_post_meta ( $post_ID, '_mb_rezension_autor_nn', true ) != '' )
        {
            echo get_post_meta ( $post_ID, '_mb_rezension_autor', true );        // Autor Nachname
        }
    }

    if ( $column_name == 'buch_erscheinungsdatum' )
    {
        if ( get_post_meta( $post_ID, '_mb_rezension_timestamp', true ) != '' )
        {
            echo get_post_meta ( $post_ID, '_mb_rezension_datum', true );       // Datum(Monat Jahr)
        }
    }
}

// new column sort order
add_filter( 'request', 'rezensionen_column_orderby' );
function rezensionen_column_orderby( $vars )
{

    if ( isset( $vars['orderby'] ) && 'Buch-Info' == $vars['orderby'] )
    {
        $vars = array_merge( $vars, array(
            'meta_key' => '_mb_rezension_aktiv',
            'orderby' => 'meta_value'
        ) );
    }

    if ( isset( $vars['orderby'] ) && 'Autor' == $vars['orderby'] )
    {
        $vars = array_merge( $vars, array(
            'meta_key' => '_mb_rezension_autor_nn',
            'orderby' => 'meta_value'
        ) );
    }

    if ( isset( $vars['orderby'] ) && 'Erscheinungsdatum' == $vars['orderby'] )
    {
        $vars = array_merge( $vars, array(
            'meta_key' => '_mb_rezension_timestamp',
            'orderby' => 'meta_value_num'
        ) );
    }

    return $vars;
}

/* eof */
?>