<?php

add_action('init', 'create_cpt_interviews');

function create_cpt_interviews() {

  register_post_type('interviews',
    array(
      'labels'      => array(
        'name'                => __( 'Interviews', TEXTDOMAIN ),
        'singular_name'       => __( 'Interview', TEXTDOMAIN ),
        'add_new'             => __( 'Neus Interview schreiben', TEXTDOMAIN ),
        'add_new_item'        => __( 'Neues Interview', TEXTDOMAIN ),
        'edit_item'           => __( 'Interview bearbeiten', TEXTDOMAIN ),
        'new_item'            => __( 'Neues Interview', TEXTDOMAIN ),
        'all_items'           => __( 'Alle Interviews', TEXTDOMAIN ),
        'view_item'           => __( 'Interview anzeigen', TEXTDOMAIN ),
        'search_items'        => __( 'Interview suchen', TEXTDOMAIN ),
        'not_found'           => __( 'Kein passendes Interview gefunden', TEXTDOMAIN ),
        'not_found_in_trash'  => __( 'Kein Interview im Papierkorb gefunden', TEXTDOMAIN ),
        'parent_item_colon'   => '',
        'menu_name'           => __( 'Interviews', TEXTDOMAIN )
      ),
      'public'              => true,
      'has_archive'         => true,
      'rewrite'             => array(
        'slug'  => 'interviews',
        'feeds' => true
      ),
      'publicly_queryable'  => true,
      'show_ui'             => true, 
      'show_in_menu'        => true, 
      'query_var'           => true,
      'capability_type'     => 'post',
      'hierarchical'        => false,
      'menu_position'       => 5,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields', 'revisions'  ),
      //'menu_icon'           => '',
      'taxonomies' => array('post_tag','category')
      
    )
    
  ); 

}

add_action('add_meta_boxes', 'mb_interviews_infos_create');

function mb_interviews_infos_create() {
    add_meta_box('mb_interviews_infos', 'Personeninfos','mb_interviews_content', 'interviews', 'normal', 'high' );
}

function mb_interviews_content($post) {
    $mb_interview_aktiv           = get_post_meta ($post->ID, '_mb_interview_aktiv', true);           // Buchinfobox aktivieren
    $mb_interview_autor           = get_post_meta ($post->ID, '_mb_interview_autor', true);           // Autor
?>

    <div class="buchinfo-item">
        <label for="mb_interview_aktiv"><?php echo __( 'Personinfobox aktivieren: ', TEXTDOMAIN ); ?></label>

        <input type="radio" name="mb_interview_aktiv" <?php if($mb_interview_aktiv == 'on') echo 'checked="checked"'; ?> value="on" onClick="jQuery('#buchinfo').removeClass('no-display');"> Ja
        <input type="radio" name="mb_interview_aktiv" <?php if($mb_interview_aktiv != 'on') echo 'checked="checked"'; ?> value="off" onClick="jQuery('#buchinfo').addClass('no-display');"> Nein

    </div>
    <div id="buchinfo" class="<?php if($mb_interview_aktiv != 'on') echo 'no-display'; ?>">
        <hr>

        <div class="buchinfo-item">
            <label for="mb_interview_autor"><?php echo __( 'Name: ', TEXTDOMAIN ); ?></label>
            <input type="text" id="mb_interview_autor" name="mb_interview_autor" value="<?php echo esc_attr( $mb_interview_autor );?>"/>
        </div>

        <div class="clear"></div>

    </div>
<?php
}

add_action ('save_post', 'mb_interviews_save_meta');

function mb_interviews_save_meta($post_id) {
    if (isset($_POST['mb_interview_aktiv'])) {
        update_post_meta($post_id, '_mb_interview_aktiv', strip_tags($_POST['mb_interview_aktiv']));
    }

    if (isset($_POST['mb_interview_autor'])){
        update_post_meta($post_id, '_mb_interview_autor', strip_tags($_POST['mb_interview_autor']));
    }
}

/* eof */

?>