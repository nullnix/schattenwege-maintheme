<?php

add_action('init', 'create_cpt_tagebuch');

function create_cpt_tagebuch() {

  register_post_type('tagebuch',
    array(
      'labels'      => array(
        'name'                => __( 'Tagebuch', TEXTDOMAIN ),
        'singular_name'       => __( 'Tagebuch', TEXTDOMAIN ),
        // 'add_new'             => __( 'Neuen Eintrag schreiben', TEXTDOMAIN ),
        // 'add_new_item'        => __( 'Neuer Eintrag', TEXTDOMAIN ),
        // 'edit_item'           => __( 'Eintrag bearbeiten', TEXTDOMAIN ),
        // 'new_item'            => __( 'Neuer Eintrag', TEXTDOMAIN ),
        // 'all_items'           => __( 'Alle Einträge', TEXTDOMAIN ),
        // 'view_item'           => __( 'Eintrag anzeigen', TEXTDOMAIN ),
        // 'search_items'        => __( 'Eintrag suchen', TEXTDOMAIN ),
        // 'not_found'           => __( 'Kein passenden Eintrag gefunden', TEXTDOMAIN ),
        // 'not_found_in_trash'  => __( 'Kein Eintrag im Papierkorb gefunden', TEXTDOMAIN ),
        // 'parent_item_colon'   => '',
        'menu_name'           => __( 'Tagebuch', TEXTDOMAIN )
      ),
      'public'              => true,
      'has_archive'         => true,
      'rewrite'             => array(
        'slug'  => 'tagebuch',
        'feeds' => true
      ),
      'publicly_queryable'  => true,
      'show_ui'             => true, 
      'show_in_menu'        => true, 
      'query_var'           => true,
      'capability_type'     => 'post',
      'hierarchical'        => false,
      'menu_position'       => 5,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields', 'revisions'  ),
      //'menu_icon'           => '',
      'taxonomies' => array('post_tag','category')
      
    )
    
  ); 

}

add_action( 'init', 'create_taxonomies_tagebuch', 0 );

function create_taxonomies_tagebuch() {
  // Add new taxonomy, make it hierarchical (like categories)
  

  $args = array(

      'labels'              => array(
      'name'                => _x( 'Bereich', TEXTDOMAIN ),
      'singular_name'       => _x( 'Bereich', TEXTDOMAIN ),
      'search_items'        => __( 'Bereich durchsuchen', TEXTDOMAIN ),
      'all_items'           => __( 'Alle Bereiche', TEXTDOMAIN ),
      'parent_item'         => __( 'Übergeordneter Bereich', TEXTDOMAIN ),
      'parent_item_colon'   => __( 'Übergeordneter Bereich:', TEXTDOMAIN ),
      'edit_item'           => __( 'Bereich bearbeiten', TEXTDOMAIN ),
      'update_item'         => __( 'Bereich aktuallisieren', TEXTDOMAIN ),
      'add_new_item'        => __( 'Neuer Bereich', TEXTDOMAIN ),
      'new_item_name'       => __( 'Bereich Name', TEXTDOMAIN ),
      'menu_name'           => __( 'Bereiche', TEXTDOMAIN )
      ),
    
    'label' => __( 'test', TEXTDOMAIN ),

    'hierarchical'        => true,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array(
      'slug' => 'tagebuch',
      'hierarchical' => true, // This will allow URL's like "/locations/boston/cambridge/" ),
      'with_front' => false, // Don't display the category base before "/locations/"
      ), 
    
    'capabilities'        => array(
      'assign_terms'  => 'edit_guides',
      'edit_termns'   => 'publish_guides'
    )
    
  );
  register_taxonomy( 'tagebuch', 'tagebuch' , $args );
  

}





?>