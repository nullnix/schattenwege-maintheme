<?php
class Post_Views
{
    const KEY = '_views_count';

    private static function get_post_views_count($post_id)
    {
        return get_post_meta($post_id, self::KEY, true);
    }

    public static function get_post_views($post_id)
    {
        $count = self::get_post_views_count($post_id);
        $count = $count === '' ? 0 : $count;
        return sprintf(_n('%d Aufruf', '%d Aufrufe', $count), $count);
    }

    public static function increment_post_views($post_id)
    {
        $count = self::get_post_views_count($post_id);
        if ($count === '') {
            delete_post_meta($post_id, self::KEY);
            add_post_meta($post_id, self::KEY, '0');
        } else {
            $count++;
            update_post_meta($post_id, self::KEY, $count);
        }
    }
}

function get_popular_posts_by_views($limit = 5)
{
    global $post;

    $tmp_post = $post;

    $popular_posts = get_posts(
        array(
            'post_type' => array('tagebuch', 'post', 'interviews', 'rezensionen'),
            'posts_per_page' => $limit,
            'meta_key' => Post_Views::KEY,
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
        )
    );

    $output = '<ul>';

    foreach ($popular_posts as $tmp_post) {
        $title = get_the_title($tmp_post->ID);
        $output .= '<li><a href="' . get_permalink($tmp_post->ID) . '" title="' . $title . ' - ' . Post_Views::get_post_views($tmp_post->ID) . '">' . $title . '</a></li>';
    }

    $post = $tmp_post;

    return $output . '</ul>';
}

function show_popular_posts()
{
    return get_popular_posts_by_views();
}

add_shortcode('popluar-posts', 'show_popular_posts');

?>