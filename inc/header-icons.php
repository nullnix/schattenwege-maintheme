<?php
if ( !is_home() )
{
    $socialthumb = wp_get_attachment_image_src ( get_post_thumbnail_id (), 'full', true );
}


if ( isset( $socialthumb)  )
{
    $socialthumb = $socialthumb[0];
    $socialthumb_static = get_template_directory_uri() . '/images/social-thumb.png';
}
else
{
    $socialthumb_static  = $socialthumb = get_template_directory_uri() . '/images/social-thumb.png';
}
?>

        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/png">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/png">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/png">

        <meta property="og:title" content="<?php if (is_home ()) echo ("Startseite"); else { wp_title ("&raquo;", true, "right"); bloginfo('name'); } ?>"/>
        <meta property="og:site_name" content="<?php bloginfo('name'); ?>"/>
        <meta property="og:type" content="blog"/>
        <meta property="og:url" content="<?php echo ("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>"/>
        <meta property="og:image" content="<?php echo ($socialthumb_static); ?>"/>
        <meta property="og:image" content="<?php echo ($socialthumb); ?>"/>
