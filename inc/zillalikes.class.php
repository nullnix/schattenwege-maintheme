<?php

if (class_exists('ZillaLikes')) {
    class myZillaLikes extends ZillaLikes
    {

        function __construct()
        {
            add_filter('manage_posts_columns', array(&$this, 'herzen_columns_head'));
            add_action('manage_posts_custom_column', array(&$this, 'herzen_columns_content'), 10, 2);
            add_filter('request', array(&$this, 'herzen_column_orderby'));
            add_filter('manage_edit-post_sortable_columns', array(&$this, 'herzen_columns_head'));
            add_filter('manage_edit-rezensionen_sortable_columns', array(&$this, 'herzen_columns_head'));
            add_filter('manage_edit-tagebuch_sortable_columns', array(&$this, 'herzen_columns_head'));
            add_filter('manage_edit-interviews_sortable_columns', array(&$this, 'herzen_columns_head'));
        }

        /* ADD NEW COLUMN */

        function herzen_columns_head($defaults)
        {
            $defaults['herzen'] = 'Herzen';
            return $defaults;
        }

        function herzen_columns_content($column_name, $post_ID)
        {
            if ($column_name == 'herzen') {
                $herzen = get_post_meta($post_ID, '_zilla_likes', true);
                echo $herzen;
            }
        }

        function herzen_column_orderby($vars)
        {
            if (isset($vars['orderby']) && 'Herzen' == $vars['orderby']) {
                $vars = array_merge($vars, array(
                    'meta_key' => '_zilla_likes',
                    'orderby' => 'meta_value_num'
                ));
            }

            return $vars;
        }
    }


    global $zilla_likes;
    $zilla_likes = new myZillaLikes();
}

?>