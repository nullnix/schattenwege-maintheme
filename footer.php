
            <div id="footer">
                <div class="widget-container first">
                    <?php show_sidebars(array('footer-left')); ?>
                </div>

                <div class="widget-container second">
                    <?php show_sidebars(array('footer-center')); ?>
                </div>

                <div class="widget-container last">
                    <?php show_sidebars(array('footer-right')); ?>
                </div>

                <div class="clear"></div>

                <div class="copyright">schattenwege.net &copy; 2010 - <?php echo date('Y'); ?>
                    <?php
                        if ( has_nav_menu('footer-menu') ) {
                            wp_nav_menu( array(
                                'theme_location' => 'footer-menu',
                                'container' => false,
                            ));
                    } ?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php

        wp_footer();

        ?>
        <?php if ( !is_user_logged_in() ) { ?>
        <!-- /footer -->
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-39479966-1']);
            _gaq.push(['_gat._anonymizeIp']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <?php } ?>
        </div>
    </body>
</html>