<?php
/* Template: CPT Rezensionen */
get_header();
?>
<div id="main">
    <div class="col-1">
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <h2><?php the_title(); ?></h2>

                <?php include('parts/postmeta.php'); ?>

                <?php include('parts/show-post-thumbnail-rezi.php'); ?>

                <?php
                if (get_post_meta($post->ID, '_mb_rezension_aktiv', true) == 'on') {
                ?>

                    <div class="post-meta-buchinfo">
                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_verlag', true)) {
                            echo get_post_meta($post->ID, '_mb_rezension_verlag', true) . ', ';
                        }
                        ?>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_auflage', true)) {
                            echo get_post_meta($post->ID, '_mb_rezension_auflage', true);
                        }
                        ?>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_datum', true)) {
                            echo get_post_meta($post->ID, '_mb_rezension_datum', true);
                        }
                        ?>
                        <br>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_originaltitel', true)) {
                            echo __('Originaltitel: ', TEXTDOMAIN) . get_post_meta($post->ID, '_mb_rezension_originaltitel', true);
                            echo '<br>';
                            echo get_post_meta($post->ID, '_mb_rezension_uebersetzer', true);
                            echo '<br>';
                        }
                        ?>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_art', true)) {
                            echo get_post_meta($post->ID, '_mb_rezension_art', true) . ', ';
                        }
                        ?>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_seitenzahl', true)) {
                            echo get_post_meta($post->ID, '_mb_rezension_seitenzahl', true) . ' ' . __('Seiten', TEXTDOMAIN);
                        }
                        ?>
                        <br>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_extras', true)) {
                            echo get_post_meta($post->ID, '_mb_rezension_extras', true) . '<br>';
                        }
                        ?>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_preise', true)) {
                            echo __('Preise: ', TEXTDOMAIN) . get_post_meta($post->ID, '_mb_rezension_preise', true);
                        }
                        ?>
                        <br>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_isbn', true)) {
                            echo __('ISBN: ', TEXTDOMAIN) . get_post_meta($post->ID, '_mb_rezension_isbn', true);
                        }
                        ?>
                        <br>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_leseprobe', true)) {
                            echo '<a href="' . get_post_meta($post->ID, '_mb_rezension_leseprobe', true) . '" target="_blank" title="' . __('Leseprobe', TEXTDOMAIN ). '">' . __('Leseprobe', TEXTDOMAIN) . '</a><br>';
                        }
                        ?>
                        <br>

                        <?php
                        if (get_post_meta($post->ID, '_mb_rezension_genre', true)) {
                            echo __('Genre: ', TEXTDOMAIN) . get_post_meta($post->ID, '_mb_rezension_genre', true);
                            echo '<br>';
                        }
                        ?>
                        <br>

                        <?php
                        $posttags = get_the_tags();

                        if ($posttags) {
                        ?>
                            <div class="rezi-tag-box">
                                Schlagworte:
                                <?php
                                foreach ($posttags as $tag) {
                                    echo '<a href="' . get_tag_link($tag->term_id) . '" title="' . $tag->name . '">' . $tag->name . '</a>, ';
                                }
                                ?>

                            </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="clear"></div>
                <?php
                }
                ?>

                <?php the_content(); ?>

                <?php
                if (get_post_meta($post->ID, '_mb_rezension_aktiv', true) == 'on') {
                ?>

                    <div class="post-meta-bewertung ">

                        <strong>Wertung: </strong>
                        <?php

                        $wert = get_post_meta($post->ID, '_mb_rezension_b_all', true);

                        $image = '<img style="display:inline-block;" title="schwerter_2" alt="Schwerter" src="http://www.schattenwege.net/bilder/schwerter_2.png" width="25" height="25">';
                        $image2 = '<img style="display:inline-block;" title="schwerter_1_links" src="http://www.schattenwege.net/bilder/schwerter_1_links.png" alt="Schwert" width="25" height="25">';

                        $i = 1;
                        while ($i <= floor($wert)) {
                            $i++;
                            echo $image;
                        }

                        if (strchr($wert, '.5')) {
                            echo $image2;
                        };

                        ?>

                        <br><br>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_informationsgehalt', true) != 0) { ?>
                            <em>Informationsgehalt:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_informationsgehalt', true); // bewertung preise leistung ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_verstaendlichkeit', true) != 0) { ?>
                            <em>Verständlichkeit:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_verstaendlichkeit', true); // bewertung preise leistung ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_handlung', true) != 0) { ?>
                            <em>Handlung:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_handlung', true); // bewertung handlung ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_lyrik', true) != 0) { ?>
                            <em>Lyrik:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_lyrik', true); // bewertung handlung ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_charaktere', true) != 0) { ?>
                            <em>Charaktere:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_charaktere', true); // bewertung charakter ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_lesespass', true) != 0) { ?>
                            <em>Lesespaß:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_lesespass', true); // bewertung lesespaß ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_preisleistung', true) != 0) { ?>
                            <em>Preis/Leistung:</em> <?php echo get_post_meta($post->ID, '_mb_rezension_b_preisleistung', true); // bewertung preise leistung ?>
                            / 5<br>
                        <?php } ?>

                        <?php if (get_post_meta($post->ID, '_mb_rezension_b_freitext', true) != '') { ?>
                            <?php echo get_post_meta($post->ID, '_mb_rezension_b_freitext', true); // bewertung freitext ?>
                            <br>
                        <?php } ?>

                    </div>

                    <?php if( function_exists('zilla_likes') ) zilla_likes(); ?>

                    <?php // other books
                    $args_rezis = array(
                        'post_type' => 'rezensionen',
                        'post_status' => 'publish',
                        'orderby' => 'meta_value',
                        'key' => '_mb_rezension_timestamp',
                        'order' => 'ASC',
                        'post__not_in' => array($post->ID),
                        'meta_query' => array(
                            array(
                                'key' => '_mb_rezension_autor' ,
                                'value' => get_post_meta($post->ID, '_mb_rezension_autor', true),
                            )
                        ),
                    );

                    $args_interviews = array(
                        'post_type' => 'interviews',
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'post__not_in' => array($post->ID),
                        'meta_query' => array(
                            array(
                                'key' => '_mb_interview_autor',
                                'value' => get_post_meta($post->ID, '_mb_rezension_autor', true),
                            )
//                            array(
//                                'key' => '_mb_rezension_autor' ,
//                                'value' => get_post_meta($post->ID, '_mb_rezension_autor', true),
//                            )
                        ),
                    );

                    $interviewsfromautor = new WP_Query($args_interviews);
                    $otherbooksfromautor = new WP_Query($args_rezis);


                    if ( ($interviewsfromautor->found_posts >= 1) or ($otherbooksfromautor->found_posts >= 1) ) {

                    ?>
                        <hr>
                        <div class="otherbooksfromauthor ">
                            <?php
                            if ($otherbooksfromautor->have_posts()) :

                                while ($otherbooksfromautor->have_posts()) : $otherbooksfromautor->the_post();
                            ?>
                                    <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>"><?php echo get_post_meta($post->ID, '_mb_rezension_autor', true) ?> : <?php echo get_post_meta($post->ID, '_mb_rezension_titel', true); ?></a><br>
                            <?php
                                endwhile;
                            endif;

                            if ($interviewsfromautor->have_posts()) :

                                while ($interviewsfromautor->have_posts()) : $interviewsfromautor->the_post();
                            ?>
                                    <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></a><br>
                            <?php
                                endwhile;
                            endif;
                            ?>

                        </div>
                    <?php

                    }

                    wp_reset_postdata();

                }


                ?>

                <?php include('parts/post-nav.php'); ?>

                <div id="related_rezensionen">

                    <?php //for use in the loop, list 5 post titles related to first tag on current post
                    $tags = wp_get_post_tags($post->ID);

                    if ($tags) {
                        foreach ($tags as $tag) {
                            $termids[] = $tag->term_id;
                        }

                        $args = array(
                            'post_type'      => 'rezensionen',
                            'orderby'        => 'rand',
                            'tag__in'        => $termids,
                            'post__not_in'   => array($post->ID),
                            'posts_per_page' => 5,
                        );

                        $related = new WP_Query($args);

                        $related->have_posts();

                        if ($related->have_posts()) {
                            ?>
                            <strong>Ähnliche Bücher</strong>
                            <br>
                            <?php
                            while ($related->have_posts()) : $related->the_post();

                                if (has_post_thumbnail()) {
                                    ?>
                                    <div class="post-thumbnail-container">
                                        <a href="<?php echo get_permalink(get_the_ID()); ?>" title="<?php echo get_the_title(); ?>">
                                            <?php
                                            $args = array(
                                                'alt'	=> trim(strip_tags( get_the_title() )),
                                                'title'	=> trim(strip_tags( get_the_title() )),
                                            );
                                            the_post_thumbnail('cpt-rezi-image-related', $args); ?>
                                        </a>
                                    </div>
                                <?php
                                }

                                ?>

                            <?php
                            endwhile;
                        }
                        wp_reset_postdata();
                        echo '<br class="clear">';

                    }
                    ?>

                </div>
                <br>

                <?php comments_template(); // Get wp-comments.php template ?>

            <?php endwhile; endif; // /Loop ?>
        </div>
    </div>

    <div id="sidebar">
        <?php show_sidebars(array('actionsidebar', 'rezensionensidebar', 'standard')); ?>
    </div>

<div class="clear"></div>
</div>
<?php
if ( !is_user_logged_in() ) {
    Post_Views::increment_post_views(get_the_ID());
}
?>
<?php get_footer(); ?>           