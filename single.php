<?php get_header(); ?>
      <div id="main">
        <div class="col-1">
          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            
              <?php include('parts/postmeta.php'); ?>

              <?php the_content(); ?>
            
              <div class="postmetaend">
                Kategorie: ...
              </div>
            
            <?php comments_template(); // Get wp-comments.php template ?>
            
            <?php endwhile; endif; ?>

            <?php include('parts/post-nav.php');?>

          </div>
        </div>
        
        <div id="sidebar">
          <?php show_sidebars(array('actionsidebar', 'postsidebar', 'standard')); ?>
        </div>
        
        <div class="clear"></div>
      </div>
<?php
if ( !is_user_logged_in() ) {
    Post_Views::increment_post_views(get_the_ID());
}
?>
<?php get_footer(); ?>